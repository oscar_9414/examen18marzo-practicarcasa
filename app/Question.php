<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function exams()
    {
        return $this->belongsToMany(Exam::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }
}
