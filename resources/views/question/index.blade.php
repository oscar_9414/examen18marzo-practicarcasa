@extends('layouts.app')

@section('content')
    <h1 class="alert alert-info">lista de preguntas</h1>
    <a class="btn btn-primary" href="/questions/create">Crear una nueva pregunta</a>
    <table class="table">
        <tr>
            <td>texto</td>
            <td>nombre modulo</td>
            <td>acciones</td>
        </tr>
        @foreach($questions as $question)
        <tr>
            <td>{{$question->text}}</td>
            <td>{{$question->module->name}}</td>
            <td><a class="btn btn-primary" href="/questions/{{$question->id}}">ver</a></td>

        @endforeach
        </tr>

    </table>
    <div>{{ $questions->render() }}</div>

@endsection
