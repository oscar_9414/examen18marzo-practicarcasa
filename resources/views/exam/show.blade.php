@extends('layouts.app')

@section('content')
    <h1 class="alert alert-info">Detalle del examen: {{$exam->title}}</h1>
    <table class="table">
        <tr>
            <td>Titulo</td>
            <td>Fecha</td>
            <td>Usuario</td>
            <td>Modulo</td>
        </tr>
        <tr>
            <td>{{$exam->title}}</td>
            <td>{{$exam->date}}</td>
            <td>{{$exam->user->name}}</td>
            <td>{{$exam->module->name}}</td>
        </tr>

    </table>
    <h1 class="alert alert-info">Preguntas de este examen.</h1>
    <table class="table">
    <tr>
      <td>Pregunta</td>
      <td>A</td>
      <td>B</td>
      <td>C</td>
      <td>D</td>
      <td>Correcta</td>
    </tr>
    <tr>
      <td>{{$question->text}}</td>
      <td>{{$question->a}}</td>
      <td>{{$question->b}}</td>
      <td>{{$question->c}}</td>
      <td>{{$question->d}}</td>
      <td>{{$question->answer}}</td>
    </tr>
  </table>
@endsection
