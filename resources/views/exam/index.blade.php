@extends('layouts.app')

@section('content')
    <h1 class="alert alert-info">lista de examenes</h1>
    <a class="btn btn-primary" href="/exams/create">Crear un examen</a>
    <table class="table">
        <tr>
            <td>Titulo</td>
            <td>Fecha</td>
            <td>Usuario</td>
            <td>Modulo</td>
            <td>Acciones</td>
        </tr>
        @foreach($exams as $exam)
        <tr>
            <td>{{$exam->title}}</td>
            <td>{{$exam->date}}</td>
            <td>{{$exam->user->name}}</td>
            <td>{{$exam->module->name}}</td>
            <td><a class="btn btn-primary" href="/exams/{{$exam->id}}">ver</a></td>

        @endforeach
        </tr>

    </table>
    <div>{{ $exams->render() }}</div>

@endsection
